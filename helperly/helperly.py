#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import seaborn as sns
import pandas as pd
import numpy as np
import time
import matplotlib as mpl

TORNEO_RCPARAMS = {
	'axes.facecolor': 'white',
	'font.family': 'open-sans',
	'axes.edgecolor': '#2E3C4D',
	'xtick.color': '#2E3C4D',
	'ytick.color': '#2E3C4D',
	'grid.linewidth': 0.5
}
TORNEO_COLORS = ["#2E3C4D","#F15B24","#0E71A3", "#52C2B8","#42BAE1", "#95a5a6"]


class Vizualizer(object):
	"""
	Object for data visualization methods
	"""
	def __init__(self, **kwargs) -> None:
		self.load_defaults(**kwargs)

	def load_defaults(self,
					  rcParams: dict=TORNEO_RCPARAMS,
					  colors:list=TORNEO_COLORS
					  ) -> None:

		mpl.rcParams.update(rcParams)
		self.set_palette(colors)
		pass

	def set_palette(self, colors: list) -> None:
		sns.set_palette(colors)
		pass

	def _plot(self):
		pass


class DataProcessor(object):
	"""
	Object for data processing methods
	"""
	def __init__(self, **kwargs) -> None:
		self.load_defaults(**kwargs)

	def load_defaults(self,
					  max_rows: int=500,
					  max_columns: int=500,
					  nap: float=1.1
					  ) -> None:
		"""
		Initialize default parameters for data processing.
		Params:
		-------
			- max_rows: pandas option
			- max_columns: pandas option
			- napper: napping default for timing
		"""
		pd.set_option('max_columns', max_columns)
		pd.set_option('max_rows', max_rows)
		self.napper = nap


# Check type hints:
#   - DataProcessor.load_defaults.__annotations__
DataProcessor(max_rows=400, max_columns=400)
Vizualizer(rcParams=TORNEO_RCPARAMS, colors=TORNEO_COLORS)
