from setuptools import setup

setup(name='helperly',
	version='0.1.0',
	description='Helperly',
	url='https://gitlab.com/calestini/helperly',
	author='Lucas Calestini',
	author_email='',
	license='MIT',
	packages=['helperly'],
	zip_safe=False)
