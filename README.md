# Helperly

Small package with my default preferences for plotting and data processing.

# Classes

## `Vizualizer`

- `rcParams` from `matplotlib`
- `set_palette` from `seaborn` using colors provided

## `DataProcessor`

- `set_option` from `pandas`
- `napper`: _napping_ default pausing time using `time`
